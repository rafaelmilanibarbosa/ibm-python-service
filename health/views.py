from django.http import JsonResponse
import logging

logger = logging.getLogger(__name__)


# Create your views here.
def health(request):
    """
    Check status of each external service.
    Remember to keep everything lightweight and add short timeouts
    """
    result = {'status': 'ok'}
    logger.info('Performing healthcheck')

    # Check DB making a lightweight DB query
    result['db'] = {'status': 'ok'}

    logger.debug('Healtchcheck result {}'.format(result))

    status_code = 200
    if result['status'] != 'ok':
        logger.error('Healthcheck result is bad')
        status_code = 500
    else:
        logger.info('Healtchcheck result is ok')

    response = JsonResponse(result)
    response.status_code = status_code
    return response
