from django.test import TestCase
from django.urls import reverse


class HelloTest(TestCase):

    def test_hello(self):
        url = reverse('hello')
        response = self.client.get(url)
        assert response.status_code == 200

        # Check structure
        result = response.json()
        assert result == {'status': 'ok', 'hello': {'status': 'ok'}}
